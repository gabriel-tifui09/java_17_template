import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@DisplayName("CI Cd Test Suite")
public class CiCdTest {

    @Tag(value = "main")
    @Test
    @DisplayName("CI CD Test")
    void testCiCd() {
        assertTrue(true);
    }

    @Tag(value = "failing")
    @Test
    void failingTest() {
        fail();
    }
}
